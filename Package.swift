import PackageDescription

#if os(OSX)
    let gtkUrl = "CGtk/osx"
#elseif os(Linux)
    let gtkUrl = "CGtk/linux"
#else
    fatalError("Unsupported platform.")
#endif


let package = Package(
    name: "SwiftGtk",
    dependencies: [
        .Package(url: gtkUrl, majorVersion: 1)
    ]
    
)
