import XCTest
@testable import SwiftGtk

class SwiftGtkTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(SwiftGtk().text, "Hello, World!")
    }


    static var allTests : [(String, (SwiftGtkTests) -> () throws -> Void)] {
        return [
            ("testExample", testExample),
        ]
    }
}
